if (process.env.NODE_ENV === 'development') {
    require('webpack-serve-overlay');
}

import React from 'react';
import ReactDOM from 'react-dom';
import RenderErrorOverlay from 'react-schema-renderer/components/RenderErrorOverlay/RenderErrorOverlay';
import Root from 'react-schema-renderer/Root';

ReactDOM.render(
    <RenderErrorOverlay>
        <Root />
    </RenderErrorOverlay>,
    document.getElementById('root')
);

// region debug control functions
/**
 * Shows the current debug namespaces that are enabled.
 *
 * @return {string}
 */
window.showDebug = () => localStorage.debug;
/**
 * Adds the given `namespace` to the debug property on `localStorage`.
 *
 * @param {string} namespace
 *
 * @return {string}
 */
window.addToDebug = namespace => localStorage.debug = (localStorage.debug || '')
    .split(',')
    .filter(space => space !== namespace)
    .concat(namespace) // ensure no double ups
    .join(',');
/**
 * Removes the given `namespace` from the debug property on `localStorage`.
 *
 * @param {string} namespace
 *
 * @return {string}
 */
window.removeFromDebug = namespace => localStorage.debug = (localStorage.debug || '')
    .split(',')
    .filter(space => space !== namespace)
    .join(',');
// endregion
