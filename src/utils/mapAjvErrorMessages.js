// region typedefs
/**
 * @typedef {Object} MappedAjvError
 * @property {ajv.ErrorObject} ajvError the original error provided by ajv
 * @property {string} keyword the keyword that triggered this error.
 * @property {string} dataPath accessor path to the error-triggering property in the object that was passed to ajv.
 * @property {string} message the actual error message
 */

/**
 * Collection of ajv errors, mapped to the keywords that triggered each error.
 *
 * All the errors are also stored in a `'*'` property, that can be used to access all errors.
 *
 * @typedef {Object<UiSchema.JSONv6Key|string, MappedAjvError>} MappedAjvKeywordErrors
 */

/**
 * Collection of ajv errors, mapped to the properties that caused them, followed by the keywords of each error.
 *
 * All the errors are also stored in a `'*'` property, that can be used to access all errors.
 *
 * @typedef {Object<string, MappedAjvKeywordErrors>} MappedAjvDataErrors
 */
// /** @typedef {AjvErrors.MappedError} MappedAjvError */
// /** @typedef {AjvErrors.MappedKeywordErrors} MappedAjvKeywordErrors */
// /** @typedef {AjvErrors.MappedDataErrors} MappedAjvDataErrors */
// endregion
// region private stuff
/**
 * Ensures that the given `object` has the given `property`.
 *
 * If the given `property` doesn't exist on the given `object`,
 * then `value` is assigned to the `object` with that `property`.
 *
 * This method mutates the object, via pass-by-reference.
 *
 * @param {Object} object
 * @param {string} property
 * @param {PropertyValue} value
 *
 * @return {PropertyValue}
 *
 * @template PropertyValue
 */
const ensureObjectHasProperty = (object, property, value) => object[property] || (object[property] = value);

/**
 * Follows a chain of property accessors down an object.
 *
 * @todo: support traversing arrays & such (like with botman-webclient)
 *
 * @param {Object} object
 * @param {Array<string>} chain
 *
 * @return {*}
 */
const followAccessChain = (object, chain) => chain.reduce((obj, property) => obj[property], object);

/**
 * Follows a schema path minus the keyword at the end,
 * providing the surrounding schema containing what the path points to.
 *
 * @param {UiSchema.JSONv6} uiSchema
 * @param {string} schemaPath
 *
 * @return {UiSchema.JSONv6}
 */
const followSchemaPathMinusKeyword = (uiSchema, schemaPath) => followAccessChain(uiSchema, schemaPath.split('/').slice(1, -1));

/**
 * Finds the data path for accessing the property in the data that resulted in the given ajv error.
 *
 * The data path is *usually* just the value of the `dataPath` property on the error,
 * but in a few situations this won't do - for example the `required` property won't
 * have the actual property that was missing in it's dataPath.
 *
 * While this does technically make sense from the perspective of JSON validation,
 * for how we want to use it, it doesn't, and so we correct accordingly.
 *
 * @param {UiSchema.JSONv6} uiSchema
 * @param {ajv.ErrorObject} ajvError
 */
const findErrorDataPath = (uiSchema, ajvError) => {
    if (ajvError.keyword === 'required') {
        const nameOfMissingProperty = (/** @type {ajv.RequiredParams} */ajvError.params).missingProperty;

        return `${ajvError.dataPath}/${nameOfMissingProperty}`;
    }

    return ajvError.dataPath;
};

// region templating error messages
/**
 * Finds all the templates within a given string.
 *
 * A template is defined as a substring starting with `${` & ending with `}`,
 * with a string in between like so like so: `${<...>}`
 *
 * @param {string} string
 *
 * @return {Array<string>}
 */
const findStringTemplates = string => string.match(/\${.*?}/g) || [];

/**
 * Gets the property accessor chain from a string template.
 *
 * This is expecting a string that looks like `${root.property.field}`.
 *
 * @param {string} templateString
 *
 * @return {Array<string>}
 */
const getAccessChainForTemplate = templateString => templateString.slice(2, -1).split('.');

/**
 * Follows an accessor template down an object, accessing it's properties.
 * @param {string} accessorTemplate
 * @param {Object} accessorObject
 *
 * @return {*}
 */
const followAccessorTemplate = (accessorTemplate, accessorObject) => followAccessChain(
    accessorObject,
    getAccessChainForTemplate(accessorTemplate)
);

/**
 * Expands a string, replacing any encountered accessor-templates within the string with
 * the result of applying the accessor chain on the given `obj`.
 *
 * An accessor-template is denoted like so: `${dot.separated.accessor.chain}`.
 *
 * @param {string} string the string to replace templates in.
 * @param {Object} obj The object to access via templates within the given string.
 *
 * @return {string}
 */
const expandTemplatedString = (string, obj) => {
    if (typeof string !== 'string') {
        return string;
    }

    const templates = findStringTemplates(string);

    return templates.reduce((s, template) => s.replace(template, followAccessorTemplate(template, obj)), string);
};
// endregion

/**
 * Finds the raw (unexpanded) error message to use for the given `ajvError`.
 *
 * `surroundingSchema` is listed as a parameter so that it can be passed without double up.
 * If not provided, it'll be assigned the value of the return of `followSchemaPathMinusKeyword`.
 *
 * @param {UiSchema.JSONv6} uiSchema
 * @param {ajv.ErrorObject} ajvError
 * @param {UiSchema.JSONv6} [surroundingSchema] the schema surrounding the property that errored.
 *
 * @return {?string}
 */
const findRawErrorMessage = (
    uiSchema,
    ajvError,
    surroundingSchema = followSchemaPathMinusKeyword(uiSchema, ajvError.schemaPath)
) => {
    const atRoot = uiSchema === surroundingSchema;

    const uiMessages = surroundingSchema.uiMessages;

    // todo: clean up major
    // todo: be more recursive
    if (!uiMessages) {
        return atRoot
            ? ajvError.message
            : findRawErrorMessage(uiSchema, ajvError, uiSchema);
    } // if we're at the root, use what ajv gave us - otherwise fall up for more

    if (typeof uiMessages === 'string') {
        return (/** @type {string} */uiMessages);
    } // if the message is a string, use it directly

    const keywordMessage = uiMessages[ajvError.keyword] || uiMessages['*'];

    if (!keywordMessage) {
        return atRoot
            ? ajvError.message
            : findRawErrorMessage(uiSchema, ajvError, uiSchema);
    } // if we're at the root, use what ajv gave us - otherwise fall up for more

    return keywordMessage;
};

/**
 * Gets the error message to use for the given `ajvError`,
 * based on properties within the given `uiSchema`.
 *
 * @param {UiSchema.JSONv6} uiSchema
 * @param {ajv.ErrorObject} ajvError
 * @param {Object} [other={}]
 *
 * @return {string}
 */
const getMessageForError = (uiSchema, ajvError, other = {}) => {
    const surroundingSchema = followSchemaPathMinusKeyword(uiSchema, ajvError.schemaPath);

    return expandTemplatedString(
        findRawErrorMessage(uiSchema, ajvError, surroundingSchema),
        {
            root: uiSchema,
            property: surroundingSchema,
            params: ajvError.params,
            error: ajvError,
            other
        }
    );
};
// endregion

/**
 *
 * @param {UiSchema.JSONv6} uiSchema
 * @param {ajv.ErrorObject} ajvError
 *
 * @return {MappedAjvError}
 */
const mapAjvError = (uiSchema, ajvError) => {
    const dataPath = findErrorDataPath(uiSchema, ajvError);
    const message = getMessageForError(uiSchema, ajvError, { dataPath });

    return {
        keyword: ajvError.keyword,
        dataPath,
        ajvError,
        message
    };
};

/**
 * TODO AND STUFF:
 *
 * ->> support '?' in template accessor! might require the function to become multiline, but easily doable!
 * ->> $refs is a bit broken! schemaPath isn't always correct...
 * ->> Array items and stuff is a bit broken? it's to do with their schemaPath...
 */

/**
 * Maps `ajvErrors` to an object based on the data path of the properties
 * that were passed in for validation, that resulted in the errors.
 *
 * @param {UiSchema.JSONv6} uiSchema
 * @param {Array<ajv.ErrorObject>} ajvErrors
 *
 * @return {MappedAjvDataErrors|AjvErrors.MappedDataErrors}
 */
export const mapAjvErrors = (uiSchema, ajvErrors) => {
    const mappedErrors = {};

    ajvErrors.forEach(ajvError => {
        const mappedError = mapAjvError(uiSchema, ajvError);

        ensureObjectHasProperty(
            ensureObjectHasProperty(
                mappedErrors,
                mappedError.dataPath,
                { '*': [] }
            ),
            mappedError.keyword,
            []
        ).push(mappedError);

        // add this error to the collection of all errors for this dataPath
        mappedErrors[mappedError.dataPath]['*'].push(mappedError);
    });

    return mappedErrors;
};

/**
 * Gets the highest priority error from a collection of errors,
 * that have been mapped by their keyword.
 *
 * @todo: support 'merging' messages - in particular for type, to improve 'oneOf' support
 *
 * @param {MappedAjvKeywordErrors} mappedKeywordErrors
 *
 * @return {MappedAjvError|AjvErrors.MappedError}
 */
export const getHighestPriorityKeywordError = mappedKeywordErrors => {
    return (
        mappedKeywordErrors['required']
        || mappedKeywordErrors['enum']
        || mappedKeywordErrors['type']
        || mappedKeywordErrors['*']
    )[0];
};
