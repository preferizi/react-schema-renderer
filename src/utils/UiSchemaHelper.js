const capitalize = require('capitalize');

const transformKeyIntoTitleCache = {};

class UiSchemaHelper {
    /**
     * Checks if the given `uiType` is one of the given `uiTypes`.
     *
     * @param {Array<UiSchema.UiTypeName>} uiTypes
     * @param {UiSchema.UiTypeName} uiType
     *
     * @return {boolean}
     */
    static isOfType(uiTypes, uiType) {
        return uiTypes.includes(uiType);
    }

    /**
     * Checks if this `SelectProperty` should display the selected item if its value is empty.
     *
     * @param {UiSchema.JSONv6} uiSchema
     *
     * @return {boolean}
     * @todo: blah, fix up
     */
    static shouldDisplayEmptyItems(uiSchema) {
        // todo: tie to ui property ('uiPlaceholder')
        return false;
    }

    /**
     * Transforms a property key into a title.
     *
     * This is done by replacing all `-` & `_` characters with spaces,
     * capitalising the first letter of the key, and ensuring there are
     * only single spaces in the string.
     *
     * @param {string} keyName the key name to transform into a title.
     *
     * @return {string}
     */
    static transformKeyIntoTitle(keyName) {
        return transformKeyIntoTitleCache[keyName] || (transformKeyIntoTitleCache[keyName] = capitalize(
            keyName
                .replace(/[-_]/g, ' ')
                .replace(/ * /g, ' ')
                .trim()
        ));
    }

    /**
     * Gets the `uiType` of the given `uiSchema`.
     *
     * This returns the value of the `uiType` field, unless it's not defined,
     * in which case the value of the `type` field is returned.
     *
     * @param {UiSchema.JSONv6} uiSchema the schema definition
     *
     * @return {UiSchema.UiTypeName}
     */
    static getSchemaType(uiSchema) {
        return uiSchema.uiType || uiSchema.type;
    }

    /**
     * Gets the title of the given `uiSchema`.
     *
     * If the `keyName` parameter is provided, and the `uiSchema` doesn't have
     * either the `title` or `uiTitle` fields, then the key will be used.
     *
     * If the `uiTitle` field is `null`, `null` is returned.
     *
     * @param {UiSchema.JSONv6} uiSchema the schema definition
     * @param {string} [keyName] the name of the key this schema is stored against
     *
     * @return {?string}
     */
    static getSchemaTitle(uiSchema, keyName = undefined) {
        const {
            /** @type{?string} */ uiTitle,
            /** @type{?string} */ title = null
        } = uiSchema;

        if (uiTitle || uiTitle === null) {
            return uiTitle;
        }

        if (title) {
            return title;
        }

        if (keyName) {
            return UiSchemaHelper.transformKeyIntoTitle(keyName);
        }

        return null;
    }

    /**
     * Gets the description of the given `uiSchema`.
     *
     * @param {UiSchema.JSONv6} uiSchema the schema definition
     *
     * @return {?string}
     */
    static getSchemaDescription(uiSchema) {
        const {
            /** @type{?string} */ uiDescription,
            /** @type{?string} */ description = null
        } = uiSchema;

        if (uiDescription || uiDescription === null) {
            return uiDescription;
        }

        return description;
    }

    /**
     * Gets the text to use as the placeholder from the `uiPlaceholderText` property.
     *
     * @param {UiSchema.JSONv6} uiSchema
     *
     * @return {string}
     */
    static getSchemaPlaceholderText(uiSchema) {
        return uiSchema.uiPlaceholderText;
    }

    /**
     * Gets the helper text for the top-most defined property
     * in the given `uiSchema`.
     *
     * Based off the value of the `uiHelperText` field.
     *
     * @param {UiSchema.JSONv6} uiSchema
     *
     * @return {string}
     */
    static getSchemaHelperText(uiSchema) {
        return uiSchema.uiHelperText;
    }

    /**
     * Maps the `enum` property of the given `uiSchema` to a
     * key => value object map, using the `enum` & `uiEnumLabels`
     * fields.
     *
     * If the `enum` property doesn't exist, null is returned.
     *
     * @param {UiSchema.JSONv6} uiSchema
     *
     * @return {?Object<string, string>}
     */
    static mapEnum(uiSchema) {
        if (!uiSchema.enum) {
            return null;
        }

        const enumMap = {};

        console.log(uiSchema.enum);
        // todo: look into this - enum can be more types than just string or number
        uiSchema.enum.forEach(enumValue => enumMap[enumValue] = UiSchemaHelper.getEnumLabel(uiSchema, enumValue));

        return enumMap;
    }

    /**
     * Gets the label to use for the given `enumValue`,
     * from the `uiEnumLabels` property.
     *
     * If the enum doesn't exist in the field,
     * then it's returned instead.
     *
     * @param {UiSchema.JSONv6} uiSchema
     * @param {string|number} enumValue
     *
     * @return {string|number}
     *
     * @todo: circle back to this when fine-tuning: selects + placeholder = display empty values
     */
    static getEnumLabel(uiSchema, enumValue) {
        const possibleEnumValue = uiSchema.uiEnumLabels?.[enumValue];

        return possibleEnumValue === undefined
            ? enumValue
            : possibleEnumValue;
    }
}

export default UiSchemaHelper;
