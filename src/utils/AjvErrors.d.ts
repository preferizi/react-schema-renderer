import ajv = require('ajv');
import { UiSchema } from '../../UiJSONSchema6';

export declare namespace AjvErrors {
    interface MappedError {
        /**
         * The original error provided by `ajv`.
         */
        ajvError: ajv.ErrorObject;
        /**
         * The keyword that triggered this error.
         */
        keyword: string;
        /**
         * Accessor path to the error-triggering property in the object
         * that was passed to `ajv` for validation.
         */
        dataPath: string;
        /**
         * The actual error message.
         */
        message: string;
    }

    /**
     * Collection of ajv errors, mapped to the keywords that triggered each error.
     *
     * All the errors are also stored in a `'*'` property, that can be used to access all errors.
     */
    type MappedKeywordErrors = {
        [key in keyof UiSchema.JSONv6Key | string]: MappedError;
    }

    /**
     * Collection of ajv errors, mapped to the properties that caused them, followed by the keywords of each error.
     *
     * All the errors are also stored in a `'*'` property, that can be used to access all errors.
     */
    type MappedDataErrors = {
        [key: string]: MappedKeywordErrors;

        '*': Array<MappedError>;
    }
}
