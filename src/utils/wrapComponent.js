import React from 'react';

/**
 * Wraps the given `OuterComponent` around the given `InnerComponent`.
 *
 * @param {React.ComponentType} InnerComponent
 * @param {React.ComponentType} OuterComponent
 *
 * @return {function(*): *}
 */
export default (InnerComponent, OuterComponent) => args => {
    return (
        <OuterComponent {...args} >
            <InnerComponent {...args} />
        </OuterComponent>
    );
};
