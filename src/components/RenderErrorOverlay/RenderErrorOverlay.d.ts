import * as React from 'react';
import { ReactPropTypes } from 'react';
import { StyledComponentProps } from '@material-ui/core/styles/withStyles';

declare namespace RenderErrorOverlay {
    type Shape = { [P in keyof Props]: ReactPropTypes; };

    type ClassesMap = Record<ClassesKey, Object>;
    type ClassesKey =
        | 'div'
        | 'span'
        ;

    interface Props extends StyledComponentProps<ClassesKey> {
        children?: any;
    }

    interface State {
        /**
         * Stack trace provided by React as the `error` parameter in `componentDidCatch`.
         *
         * Lines are separated by '\n'.
         */
        errorStack: string;
        /**
         * Stack trace provided by React as the `info` parameter in `componentDidCatch`.
         *
         * Lines are separated by '\n'.
         */
        componentStack: string;
    }
}

/**
 * RenderErrorOverlay component.
 *
 * Uses `componentDidCatch` to catch component errors thrown during rendering,
 * and displays them on the screen in an overlay similar to `webpack-serve-overlay`.
 */
declare const RenderErrorOverlay: React.ComponentType<RenderErrorOverlay.Props>;
/// <reference path="RenderErrorOverlay.jsx" />

export default RenderErrorOverlay;
