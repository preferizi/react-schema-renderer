import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';
import React from 'react';

/**
 * RenderErrorOverlay component.
 *
 * Uses `componentDidCatch` to catch component errors thrown during rendering,
 * and displays them on the screen in an overlay similar to `webpack-serve-overlay`.
 */
@withStyles(theme => RenderErrorOverlay.styles(theme))
class RenderErrorOverlay extends React.Component {
    // region prop types
    /**
     * @inheritDoc
     *
     * @type {RenderErrorOverlay.Shape}
     */
    static propTypes = {
        classes: PropTypes.object
    };

    /**
     * @inheritDoc
     *
     * @type {Partial<RenderErrorOverlay.Props>}
     */
    static defaultProps = {};
    // endregion
    // region styles
    /**
     *
     * @type {function(Theme): RenderErrorOverlay.ClassesMap}
     */
    static styles = theme => ({
        div: {
            backgroundColor: 'rgba(0, 0, 0, 0.85)',
            color: '#e8e8e8',
            fontFamily: 'Menlo, Consolas, monospace',
            fontSize: 'large',
            padding: '2rem',
            lineHeight: '1.2',
            whiteSpace: 'pre-wrap',
            overflow: 'auto'
        },
        span: {
            color: 'red'
        }
    });

    // endregion
    // region properties
    /**
     * @type {debug.IDebugger}
     * @private
     */
    #logger = require('debug')(this.constructor.name);

    /**
     * @inheritDoc
     *
     * @type {RenderErrorOverlay.Props}
     */
    props;

    /**
     * @inheritDoc
     *
     * @type {RenderErrorOverlay.State}
     */
    state = {
        errorStack: null,
        componentStack: null
    };

    // endregion
    /**
     * Removes any blank lines from a stack trace string.
     *
     * @param {string} stackString
     *
     * @return {string}
     */
    static removeBlackLinesFromStackString(stackString) {
        return stackString.split('\n')
                          .filter(line => !!line.trim())
                          .join('\n');
    }

    // region setState override
    /**
     * @inheritDoc
     *
     * Override that lets WebStorm be clever.
     *
     * @param {Partial<RenderErrorOverlay.State>} state
     * @param [callback]
     */
    setState(state, callback) {
        super.setState(state, callback);
    }

    // endregion
    // region component lifecycle methods
    /**
     * @inheritDoc
     *
     * It's nice to know when a `RenderErrorOverlay` is mounted,
     * in case it skews errors.
     */
    componentDidMount() {
        this.#logger('componentDidMount: mounted');
    }

    /**
     * @inheritDoc
     *
     * @param {Error} error
     * @param {React.ErrorInfo} info
     */
    componentDidCatch(error, info) {
        this.#logger('componentDidCatch: captured error', error, info);

        const errorStack = this.constructor.removeBlackLinesFromStackString(error.stack);
        const componentStack = this.constructor.removeBlackLinesFromStackString(info.componentStack);

        this.setState({ errorStack, componentStack });
    }

    // endregion
    // region render & get-render-content methods
    /**
     * Gets the content to render for the error overlay.
     *
     * @return {*}
     */
    getErrorOverlayRenderContent() {
        if (!this.state.errorStack) {
            return null;
        }

        const errorStackLines = this.state.errorStack.split('\n');
        const componentStackLines = this.state.componentStack.split('\n');

        return (
            <div className={this.props.classes.div}>
                <span className={this.props.classes.span}>{errorStackLines[0].trim()}</span>
                <br />
                {errorStackLines.slice(1).join('\n')}
                <br />
                <br />
                <span className={this.props.classes.span}>{componentStackLines[0].trim()}</span>
                <br />
                {componentStackLines.slice(1).join('\n')}
            </div>
        );
    }

    render() {
        return this.getErrorOverlayRenderContent() || this.props.children;
    }

    // endregion
}

export default RenderErrorOverlay;
