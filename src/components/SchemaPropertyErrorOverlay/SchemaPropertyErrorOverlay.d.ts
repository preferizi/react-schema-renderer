import { AsShape, ReactChildProp } from 'utils';
import { ClassNameMap, StyledComponentProps } from '@material-ui/core/styles/withStyles';
import * as React from 'react';

declare namespace SchemaPropertyErrorOverlay {
    type Shape = AsShape<Props>;

    type PartialProps = Partial<Props>;
    type PartialState = Partial<State>;

    type ClassesMap = Record<ClassesKey, object>;
    type ClassesKey =
        | 'outerDiv'
        | 'innerDiv'
        | 'span'
        ;

    interface Props extends StyledComponentProps<ClassesKey>, ReactChildProp {
        classes?: Partial<ClassNameMap<ClassesKey>>;

        propertyName: string;
        manuallyErrored: boolean;
    }

    interface State {
        hasErrored: boolean
    }
}

/**
 * SchemaPropertyErrorOverlay component.
 *
 * Uses `componentDidCatch` to catch component errors thrown during rendering,
 * and displays them on the screen in an overlay similar to `webpack-serve-overlay`.
 */
declare const SchemaPropertyErrorOverlay: React.ComponentType<SchemaPropertyErrorOverlay.Props>;
/// <reference path="SchemaPropertyErrorOverlay.jsx" />

export default SchemaPropertyErrorOverlay;
