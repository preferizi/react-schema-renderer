import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';
import React from 'react';

/**
 * RenderErrorOverlay component.
 *
 * Uses `componentDidCatch` to catch component errors thrown during rendering,
 * and displays them on the screen in an overlay similar to `webpack-serve-overlay`.
 */
@withStyles(theme => SchemaPropertyErrorOverlay.styles(theme))
class SchemaPropertyErrorOverlay extends React.Component {
    // region prop types
    /**
     * @inheritDoc
     *
     * @type {SchemaPropertyErrorOverlay.Shape}
     */
    static propTypes = {
        propertyName: PropTypes.string,
        manuallyErrored: PropTypes.bool
    };

    /**
     * @inheritDoc
     *
     * @type {SchemaPropertyErrorOverlay.PartialProps}
     */
    static defaultProps = {};
    // endregion
    // region styles
    /**
     *
     * @type {function(Theme): SchemaPropertyErrorOverlay.ClassesMap}
     */
    static styles = theme => ({
        outerDiv: {
            width: '100%',
            padding: '2px',
            textAlign: 'center'
        },
        innerDiv: {
            backgroundColor: 'rgba(0, 0, 0, 0.85)',
            color: '#e8e8e8',
            fontFamily: 'Menlo, Consolas, monospace',
            fontSize: 'large',
            lineHeight: '1.2',
            whiteSpace: 'pre-wrap',
            overflow: 'auto'
        },
        span: {
            color: 'lightcoral'
        }
    });

    // endregion
    // region properties
    /**
     * @type {debug.IDebugger}
     * @private
     */
    #logger = require('debug')(this.constructor.name);

    /**
     * @inheritDoc
     *
     * @type {SchemaPropertyErrorOverlay.Props}
     */
    props;

    /**
     * @inheritDoc
     *
     * @type {SchemaPropertyErrorOverlay.State}
     */
    state = {
        hasErrored: false
    };
    // endregion
    // region setState override
    /**
     * @inheritDoc
     *
     * Override that lets WebStorm be clever.
     *
     * @param {SchemaPropertyErrorOverlay.PartialState} state
     * @param [callback]
     */
    setState(state, callback) {
        super.setState(state, callback);
    }

    // endregion
    // region component lifecycle methods
    /**
     * @inheritDoc
     *
     * It's nice to know when a `SchemaPropertyErrorOverlay` is mounted,
     * in case it skews errors.
     */
    componentDidMount() {
        this.#logger(`${this.componentDidMount.name}: mounted`);
    }

    /**
     * @inheritDoc
     *
     * @param {Error} error
     * @param {React.ErrorInfo} info
     */
    componentDidCatch(error, info) {
        this.#logger(`${this.componentDidCatch.name}: captured error`, error, info);

        this.setState({ hasErrored: true });
    }

    // endregion
    // region render & get-render-content methods
    /**
     * Gets the content to render for the error overlay.
     *
     * @return {*}
     */
    getErrorOverlayRenderContent() {
        if (!this.state.hasErrored && !this.props.manuallyErrored) {
            return null;
        }

        return (
            <div className={this.props.classes.outerDiv}>
                <div className={this.props.classes.innerDiv}>
                    <span className={this.props.classes.span}>~ unable to render {this.props.propertyName} ~</span>
                </div>
            </div>
        );
    }

    render() {
        return this.getErrorOverlayRenderContent() || this.props.children;
    }

    // endregion
}

export default SchemaPropertyErrorOverlay;
