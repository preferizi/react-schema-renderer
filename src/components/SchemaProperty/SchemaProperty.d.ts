import { AsShape, ReactChildProp } from 'utils';
import { FormControlProps } from '@material-ui/core/es/FormControl';
import { UiSchema } from 'UiJSONSchema6';
import * as React from 'react';
import { Validator } from 'react';

export declare namespace SchemaProperty {
    // region component
    /**
     * Contains definition of common props shared by `SchemaProperty` & co sub-components.
     *
     * This is all mainly to make WebStorm happy: Using `AsShape<Props>` results in a warning
     * about { ...commonPropTypes } !== That.Shape, b/c of how WebStorm handles its typings.
     *
     * By declaring `Shape` as an interface, it lets `schemaPropertyPropTypes.js` be typed,
     * while still also be understood by WebStorm when used as `...schemaPropertyPropTypes`.
     *
     * However, this solution also prevents WebStorm from seeing the connection between
     * `Props` & `Shape`, meaning that refactoring (renaming) wouldn't target both sets
     * of the same properties (i.e `props` & `propTypes`).
     *
     * To work around this, both `Shape` & `Props` extend off `Shell`, which contains
     * all the expected properties. This results in the most acceptable downside, which is
     * that WebStorm will ask you which definition of the property you want to go to when
     * navigating up the override tree.
     */
    namespace Common {
        /**
         * Shell of the props that are common to all `SchemaProperty` components.
         *
         * Defines the names of the property props, allowing easy refactoring & renaming
         * with WebStorm, along with easy maintaining of the jsdoc blocks for properties.
         *
         * In theory this could be a union, but WebStorm doesn't like that.
         */
        interface Shell {
            /**
             * The name of this property.
             */
            propertyName: any;
            /**
             * The `UiJSONSchema` definition of this property.
             */
            propertySchema: any;
            /**
             * Collection of props to be applied to the root component of all schema properties.
             *
             * Useful for 'injecting' custom behaviour, for things like editors ;)
             *
             * You can opt to use `Redux` in place of this, for a performance boost.
             */
            rootProps?: any;
            /**
             * The component to use as the root for all schema properties.
             *
             * Useful for 'injecting' custom behaviour, for things like editors ;)
             */
            rootComponent?: any;
            /**
             * Marks if the `SchemaProperty`s being rendered should act
             * as if they're currently selected, or in focus.
             *
             * This is the only prop that's explicitly for an editor-like app,
             * as it's the only nice way to allow this functionality without
             * blowing up component performance, or requiring the editor to exist
             * with the renderer itself.
             *
             * Everything else should be do-able using `rootComponent`.
             *
             * Ideally, this will be honored by the `SchemaProperty` component,
             * but it's not guaranteed.
             */
            actSelected?: any;
            /**
             * The current value of this property.
             */
            currentValue: any;
            /**
             * The current error of this property.
             */
            currentError: any;
            /**
             * Controller function to call when the value of this property changes.
             *
             * This function passes both the new value, and the name given to the property
             * to make it easier to track changes without requiring multiple functions.
             *
             * @param {SchemaProperty.PropertyValueChangeEvent} event an object containing the name and value
             */
            onValueChange: any;
        }

        interface Shape extends Shell {
            /** @inheritDoc */ propertyName: Validator;
            /** @inheritDoc */ propertySchema: Validator;
            /** @inheritDoc */ rootProps: Validator;
            /** @inheritDoc */ rootComponent: Validator;
            /** @inheritDoc */ actSelected: Validator;
            /** @inheritDoc */ currentValue: Validator;
            /** @inheritDoc */ currentError: Validator;
            /** @inheritDoc */ onValueChange: Validator;
        }

        interface Props extends Shell {
            /** @inheritDoc */ propertyName: string;
            /** @inheritDoc */ propertySchema: UiSchema.JSONv6;
            /** @inheritDoc */ rootProps?: object;
            /** @inheritDoc */ rootComponent?: React.ReactType<FormControlProps>;
            /** @inheritDoc */ actSelected?: boolean;
            /** @inheritDoc */ currentValue: string;
            /** @inheritDoc */ currentError: string;
            /** @inheritDoc */ onValueChange: (event: SchemaProperty.PropertyValueChangeEvent) => void;
        }

    }

    type Shape = AsShape<Props>;

    type PartialProps = Partial<Props>;
    type PartialState = Partial<State>;

    interface Props extends Common.Props, ReactChildProp {
        /**
         * The component to use to render this schema property
         */
        propertyComponent: React.ComponentType<Common.Props>;
        /**
         * @inheritDoc
         */
        propertyName: string;
        /**
         * @inheritDoc
         */
        propertySchema: UiSchema.JSONv6;
        /**
         * @inheritDoc
         */
        rootProps?: object;
        /**
         * @inheritDoc
         */
        rootComponent?: React.ReactType<FormControlProps>;
        /**
         * @inheritDoc
         */
        actSelected?: boolean;
        /**
         * @inheritDoc
         */
        currentValue: string;
        /**
         * @inheritDoc
         */
        currentError: string;
        /**
         * @inheritDoc
         *
         * @param {SchemaProperty.PropertyValueChangeEvent} event an object containing the name and value
         */
        onValueChange: (event: SchemaProperty.PropertyValueChangeEvent) => void;
    }

    interface State {
    }

    // endregion

    /**
     * @deprecated
     */
    type ChangeEvent<ValueType = string> = PropertyValueChangeEvent<ValueType>;

    /**
     * Event detailing the change of the `value` of a `property`.
     */
    interface PropertyValueChangeEvent<ValueType = string> {
        /**
         * The `name` given to the property whose value has changed.
         */
        name: string;
        /**
         * The `value` that the property has changed to.
         *
         * Ideally this property will be a string, but it's reasonable
         * to pass up instances of things like `File` as well.
         */
        value: ValueType;
    }

    type SchemaPropertyComponent = React.ComponentType<Common.Props>;

    /**
     * Function that tests if the given schema property should be rendered by the component
     * this function was registered with.
     */
    type UseComponentForSchemaPropertyTesterFunc = (propertyName: string, propertySchema: UiSchema.JSONv6) => boolean;

    /**
     * A record registering a component for use rendering schema properties that
     * match the criteria tested for by the registered tester function.
     */
    interface ComponentRegistrationRecord {
        component: SchemaPropertyComponent;
        tester: UseComponentForSchemaPropertyTesterFunc;
    }
}

/**
 * `SchemaProperty` Component.
 */
declare const SchemaProperty: React.ComponentType<SchemaProperty.Props>;
/// <reference path="SchemaProperty.jsx" />

export default SchemaProperty;
