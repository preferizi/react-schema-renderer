import React from 'react';
import schemaPropertySharedPropTypes from 'react-schema-renderer/components/SchemaProperty/schemaPropertySharedPropTypes';
import deepEquals from 'react-schema-renderer/utils/deepEquals';
import PropTypes from 'prop-types';

/**
 * `SchemaProperty` Component.
 */
class SchemaProperty extends React.Component {
    // region prop types
    /**
     * @inheritDoc
     *
     * @type {SchemaProperty.Shape}
     */
    static propTypes = {
        ...schemaPropertySharedPropTypes,
        propertyComponent: PropTypes.func
    };

    /**
     * @inheritDoc
     *
     * @type {SchemaProperty.PartialProps}
     */
    static defaultProps = {};
    // endregion
    // region properties
    /**
     * @type {debug.IDebugger}
     * @private
     */
    #logger = require('debug')(this.constructor.name);

    /**
     * @inheritDoc
     *
     * @type {SchemaProperty.Props}
     */
    props;

    /**
     * @inheritDoc
     *
     * @type {SchemaProperty.State}
     */
    state = {};

    // endregion
    // region setState override
    /**
     * @inheritDoc
     *
     * Override that lets WebStorm be clever.
     *
     * @param {SchemaProperty.PartialState} state
     * @param [callback]
     */
    setState(state, callback) {
        super.setState(state, callback);
    }

    // endregion
    // region component lifecycle methods
    /**
     * @inheritDoc
     *
     * @param {SchemaProperty.Props} nextProps
     * @param {SchemaProperty.State} nextState
     * @param nextContext
     *
     * @return {boolean}
     */
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return !deepEquals(
            { ...this.props },
            { ...nextProps }
        );
    }

    // endregion
    // region render & get-render-content methods
    render() {
        const name = this.props.propertyName;
        const schema = this.props.propertySchema;

        const SchemaPropertyComponent = this.props.propertyComponent;

        if (!SchemaPropertyComponent) {
            return null;
        }

        return (
            <SchemaPropertyComponent
                propertyName={name}
                propertySchema={schema}

                rootComponent={this.props.rootComponent}
                rootProps={{ ...this.props.rootProps, 'data-property-name': name }}
                actSelected={this.props.actSelected}

                currentValue={this.props.currentValue}
                currentError={this.props.currentError}

                onValueChange={this.props.onValueChange}
            />
        );
    }

    // endregion
}

export default SchemaProperty;
