import CheckboxProperty from 'react-schema-renderer/components/SchemaProperty/types/CheckboxProperty/CheckboxProperty';
import FileProperty from 'react-schema-renderer/components/SchemaProperty/types/FileProperty/FileProperty';
import SelectProperty from 'react-schema-renderer/components/SchemaProperty/types/SelectProperty/SelectProperty';
import SwitchProperty from 'react-schema-renderer/components/SchemaProperty/types/SwitchProperty/SwitchProperty';
import TextProperty from 'react-schema-renderer/components/SchemaProperty/types/TextProperty/TextProperty';

/**
 * Builds a 'common' schema property component registration record for the given component.
 *
 * This method assumes that the component has a static `shouldUseComponentForSchemaPropertyTester`
 * method, which it sets at the `tester` property of the record.
 *
 * @param {*} component
 *
 * @return {SchemaProperty.ComponentRegistrationRecord}
 *
 * @throws {Error} when the component doesn't have a static method 'shouldUseComponentForSchemaPropertyTester'
 */
export const buildRecordForComponent = component => {
    if (typeof component.shouldUseComponentForSchemaPropertyTester !== 'function') {
        throw new Error(`${component.name} missing static function called shouldUseComponentForSchemaPropertyTester`);
    }

    return { component, tester: component.shouldUseComponentForSchemaPropertyTester.bind(component) };
};

/**
 * Register of the common, build-in schema property components.
 *
 * @type {Array<SchemaProperty.ComponentRegistrationRecord>}
 */
export const schemaPropertyComponentsCommonRegister = [
    buildRecordForComponent(SelectProperty),
    buildRecordForComponent(CheckboxProperty),
    buildRecordForComponent(SwitchProperty),
    buildRecordForComponent(FileProperty),
    buildRecordForComponent(TextProperty)
];

export default schemaPropertyComponentsCommonRegister;
