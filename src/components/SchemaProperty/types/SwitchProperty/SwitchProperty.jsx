import FormControl from '@material-ui/core/es/FormControl/FormControl';
import FormControlLabel from '@material-ui/core/es/FormControlLabel/FormControlLabel';
import Switch from '@material-ui/core/es/Switch/Switch';
import withStyles from '@material-ui/core/styles/withStyles';
import autobind from 'autobind';
import React from 'react';
import schemaPropertySharedPropTypes from 'react-schema-renderer/components/SchemaProperty/schemaPropertySharedPropTypes';
import UiSchemaHelper from 'react-schema-renderer/utils/UiSchemaHelper';

/**
 * `SelectProperty` Component.
 */
@withStyles(theme => SwitchProperty.styles(theme))
class SwitchProperty extends React.Component {
    // region prop types
    /**
     * @inheritDoc
     *
     * @type {SwitchProperty.Shape}
     */
    static propTypes = {
        ...schemaPropertySharedPropTypes
    };

    /**
     * @inheritDoc
     *
     * @type {SwitchProperty.PartialProps}
     */
    static defaultProps = {};
    // endregion
    // region styles
    /**
     *
     * @type {function(Theme): SwitchProperty.ClassesMap}
     */
    static styles = theme => ({});
    // endregion
    // region properties
    /**
     * @type {debug.IDebugger}
     * @private
     */
    #logger = require('debug')(this.constructor.name);

    /**
     * @inheritDoc
     *
     * @type {SwitchProperty.Props}
     */
    props;

    /**
     * @inheritDoc
     *
     * @type {SwitchProperty.State}
     */
    state = {
        items: {}
    };
    // endregion
    // region shouldUseComponentForSchemaPropertyTester
    /**
     * Tests if this component should be used to render the given schema property.
     *
     * @param {string} propertyName
     * @param {UiSchema.JSONv6} propertySchema
     *
     * @return {boolean}
     * @static
     */
    static shouldUseComponentForSchemaPropertyTester(propertyName, propertySchema) {
        return propertySchema.uiType === 'switch';
    }

    // endregion
    // region setState override
    /**
     * @inheritDoc
     *
     * Override that lets WebStorm be clever.
     *
     * @param {SwitchProperty.PartialState} state
     * @param [callback]
     */
    setState(state, callback) {
        super.setState(state, callback);
    }

    // endregion
    /**
     * Gets the title for this components schema.
     *
     * @return {?string}
     */
    getSchemaTitle() {
        return UiSchemaHelper.getSchemaTitle(this.props.propertySchema, this.props.propertyName);
    }

    // region autobound methods
    /**
     * Handles when the value of this property changes.
     *
     * @param {React.ChangeEvent} event
     */
    @autobind
    handlePropertyValueChange(event) {
        this.props.onValueChange({ name: this.props.propertyName, value: event.target.checked });
    }

    // endregion
    // region render & get-render-content methods

    render() {
        return (
            <FormControl
                component={this.props.rootComponent}
                error={!!this.props.currentError}

                {...this.props.rootProps}
            >
                <FormControlLabel
                    control={<Switch />}

                    label={this.getSchemaTitle()}

                    checked={this.props.currentValue}

                    onChange={this.handlePropertyValueChange}
                />
            </FormControl>
        );
    }

    // endregion
}

export default SwitchProperty;
