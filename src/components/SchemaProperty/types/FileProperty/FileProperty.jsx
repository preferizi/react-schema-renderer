import FormControl from '@material-ui/core/es/FormControl/FormControl';
import FormHelperText from '@material-ui/core/es/FormHelperText/FormHelperText';
import withStyles from '@material-ui/core/styles/withStyles';
import { CloudUpload } from '@material-ui/icons';
import autobind from 'autobind';
import React from 'react';
import Dropzone from 'react-dropzone';
import schemaPropertySharedPropTypes from 'react-schema-renderer/components/SchemaProperty/schemaPropertySharedPropTypes';
import UiSchemaHelper from 'react-schema-renderer/utils/UiSchemaHelper';

const cx = require('classnames');

/**
 * `FileProperty` Component.
 */
@withStyles(theme => FileProperty.styles(theme))
class FileProperty extends React.Component {
    // region prop types
    /**
     * @inheritDoc
     *
     * @type {FileProperty.Shape}
     */
    static propTypes = {
        ...schemaPropertySharedPropTypes
    };

    /**
     * @inheritDoc
     *
     * @type {FileProperty.PartialProps}
     */
    static defaultProps = {};
    // endregion
    // region styles
    /**
     *
     * @type {function(Theme): FileProperty.ClassesMap}
     */
    static styles = theme => ({
        dropZone: {
            borderStyle: 'solid',
            borderWidth: '1px',
            borderColor: '#666666',
            height: '100px',
            textAlign: 'center',
            padding: '25px',
            // margin: '10px 0',
            cursor: 'pointer'
        },
        errorZone: {
            borderColor: 'red',
            color: 'maroon'
        }
    });
    // endregion
    // region properties
    /**
     * @type {debug.IDebugger}
     * @private
     */
    #logger = require('debug')(this.constructor.name);

    /**
     * @inheritDoc
     *
     * @type {FileProperty.Props}
     */
    props;

    /**
     * @inheritDoc
     *
     * @type {FileProperty.State}
     */
    state = {};

    // endregion
    /**
     * Tests if this component should be used to render the given schema property.
     *
     * @param {string} propertyName
     * @param {UiSchema.JSONv6} propertySchema
     *
     * @return {boolean}
     * @static
     */
    static shouldUseComponentForSchemaPropertyTester(propertyName, propertySchema) {
        return propertySchema.uiType === 'file';
    }

    // region setState override
    /**
     * @inheritDoc
     *
     * Override that lets WebStorm be clever.
     *
     * @param {FileProperty.PartialState} state
     * @param [callback]
     */
    setState(state, callback) {
        super.setState(state, callback);
    }

    // endregion

    /**
     * Gets the text to use for the placeholder.
     *
     * @return {string}
     */
    getSchemaPlaceholderText() {
        return UiSchemaHelper.getSchemaPlaceholderText(this.props.propertySchema);
    }

    /**
     * Gets the title for this components schema.
     *
     * @return {?string}
     */
    getSchemaTitle() {
        return UiSchemaHelper.getSchemaTitle(this.props.propertySchema, this.props.propertyName);
    }

    /**
     * Gets the helper text for this property definition.
     *
     * Based off the value of the `uiHelperText` field.
     *
     * @return {string}
     */
    getSchemaHelperText() {
        return UiSchemaHelper.getSchemaHelperText(this.props.propertySchema);
    }

    getDropzoneText() {
        return this.getSchemaPlaceholderText() || 'Drag or click here to upload a file';
    }

    // region autobound methods
    /**
     * Handles when the value of this property changes.
     *
     * @param {React.ChangeEvent} event
     */
    @autobind
    handlePropertyValueChange(event) {
        this.props.onValueChange({ name: this.props.propertyName, value: event.target.value });
    }

    // endregion
    // region render & get-render-content methods
    render() {
        return (
            <FormControl
                component={this.props.rootComponent}
                fullWidth

                {...this.props.rootProps}
            >
                <Dropzone
                    className={cx({
                        [this.props.classes.dropZone]: true,
                        [this.props.classes.errorZone]: !!this.props.currentError
                    })}
                >
                    <CloudUpload />
                    <p>{this.getDropzoneText()}</p>
                </Dropzone>
                <FormHelperText error={!!this.props.currentError}>
                    {this.props.currentError}
                </FormHelperText>
            </FormControl>
        );
    }

    // endregion
}

export default FileProperty;
