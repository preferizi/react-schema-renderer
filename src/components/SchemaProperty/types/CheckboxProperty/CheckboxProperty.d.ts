import { AsShape, ReactChildProp } from 'utils';
import { ClassNameMap, StyledComponentProps } from '@material-ui/core/styles/withStyles';
import { FormControlProps } from '@material-ui/core/es/FormControl';
import { SchemaProperty } from 'SchemaProperty';
import { UiSchema } from 'UiJSONSchema6';
import * as React from 'react';

declare namespace CheckboxProperty {
    type Shape = AsShape<Props>;

    type PartialProps = Partial<Props>;
    type PartialState = Partial<State>;

    type ClassesMap = Record<ClassesKey, object>;
    type ClassesKey =
        | 'myClass'
        ;

    interface Props extends SchemaProperty.Common.Props, ClassNameMap, StyledComponentProps<ClassesKey>, ReactChildProp {
        classes?: Partial<ClassNameMap<ClassesKey>>;

        /** @inheritDoc */ propertyName: string;
        /** @inheritDoc */ propertySchema: UiSchema.JSONv6;
        /** @inheritDoc */ rootProps?: object;
        /** @inheritDoc */ rootComponent?: React.ReactType<FormControlProps>;
        /** @inheritDoc */ actSelected?: boolean
        /** @inheritDoc */ currentValue: string;
        /** @inheritDoc */ currentError: string;
        /** @inheritDoc */ onValueChange: (event: SchemaProperty.PropertyValueChangeEvent) => void;
    }

    interface State {
    }
}

/**
 * `CheckboxProperty` Component.
 */
declare const CheckboxProperty: React.ComponentType<CheckboxProperty.Props>;
/// <reference path="CheckboxProperty.jsx" />

export default CheckboxProperty;
