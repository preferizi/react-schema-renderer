import MenuItem from '@material-ui/core/es/MenuItem/MenuItem';
import TextField from '@material-ui/core/es/TextField/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import autobind from 'autobind';
import React from 'react';
import schemaPropertySharedPropTypes from 'react-schema-renderer/components/SchemaProperty/schemaPropertySharedPropTypes';
import AxiosError from 'react-schema-renderer/exceptions/AxiosError';
import UiSchemaHelper from 'react-schema-renderer/utils/UiSchemaHelper';

/**
 * `SelectProperty` Component.
 */
@withStyles(theme => SelectProperty.styles(theme))
class SelectProperty extends React.Component {
    // region prop types
    /**
     * @inheritDoc
     *
     * @type {SelectProperty.Shape}
     */
    static propTypes = {
        ...schemaPropertySharedPropTypes
    };

    /**
     * @inheritDoc
     *
     * @type {SelectProperty.PartialProps}
     */
    static defaultProps = {};
    // endregion
    // region styles
    /**
     *
     * @type {function(Theme): SelectProperty.ClassesMap}
     */
    static styles = theme => ({
        selectField: {
            marginTop: '16px'
        }
    });
    // endregion
    // region properties
    /**
     * @type {debug.IDebugger}
     * @private
     */
    #logger = require('debug')(this.constructor.name);

    /**
     * @inheritDoc
     *
     * @type {SelectProperty.Props}
     */
    props;

    /**
     * @inheritDoc
     *
     * @type {SelectProperty.State}
     */
    state = {
        items: {}
    };

    // endregion
    /**
     * Tests if this component should be used to render the given schema property.
     *
     * @param {string} propertyName
     * @param {UiSchema.JSONv6} propertySchema
     *
     * @return {boolean}
     * @static
     */
    static shouldUseComponentForSchemaPropertyTester(propertyName, propertySchema) {
        return !!propertySchema.enum;
    }

    // region setState override
    /**
     * @inheritDoc
     *
     * Override that lets WebStorm be clever.
     *
     * @param {SelectProperty.PartialState} state
     * @param [callback]
     */
    setState(state, callback) {
        super.setState(state, callback);
    }

    // endregion
    // region component lifecycle methods
    /**
     * @inheritDoc
     */
    componentDidMount() {
        this.updateItems()
            .catch(error => this.#logger(`${this.componentDidMount.name}#${this.updateItems.name}: error`, error));
    }

    // endregion
    /**
     * Updates the items stored on the state.
     *
     * This method calls `setState`.
     *
     * @return {Promise<void>}
     */
    async updateItems() {
        try {
            // todo: support async requests?
            const items = UiSchemaHelper.mapEnum(this.props.propertySchema) || [];

            // console.log(items);

            this.setState({ items });
        } catch (error) {
            if (!(error instanceof AxiosError)) {
                throw error;
            }

            this.#logger(`${this.updateItems.name}: error`, error.response || error);
        }
    }

    /**
     * Checks if this `SelectProperty` should display the selected item if its value is empty.
     *
     * @return {boolean}
     */
    shouldDisplayEmptyItems() {
        return UiSchemaHelper.shouldDisplayEmptyItems(this.props.propertySchema);
    }

    /**
     * Gets the placeholder text to use for the placeholder item.
     *
     * @return {string}
     */
    getSchemaPlaceholderText() {
        return UiSchemaHelper.getSchemaPlaceholderText(this.props.propertySchema);
    }

    /**
     * Gets the helper text for this property.
     *
     * Based off the value of the `uiHelperText` field.
     *
     * @return {string}
     */
    getSchemaHelperText() {
        return UiSchemaHelper.getSchemaHelperText(this.props.propertySchema);
    }

    // region autobound methods
    /**
     * Handles when the value of this property changes.
     *
     * @param {React.ChangeEvent} event
     */
    @autobind
    handlePropertyValueChange(event) {
        this.props.onValueChange({ name: this.props.propertyName, value: event.target.value });
    }

    // endregion
    // region render & get-render-content methods
    /**
     * Gets the render content for the placeholder item of this `SelectProperty`.
     *
     * @return {*}
     */
    getPlaceholderItemRenderContent() {
        // todo: clean up - should be 'placeholder' focused
        if (!this.shouldDisplayEmptyItems()) {
            return null;
        }

        return (
            <MenuItem value="" disabled>{this.getSchemaPlaceholderText()}</MenuItem>
        );
    }

    /**
     * Gets the render content for selection items section of this component.
     *
     * This makes primary use of the `items` prop.
     *
     * @return {*}
     */
    getSelectionItemsRenderContent() {
        const items = this.state.items;

        return Object
            .keys(items)
            .map(key => (
                    <MenuItem
                        value={key}
                        key={key}
                    >
                        {items[key]}
                    </MenuItem>
                )
            );
    }

    render() {
        const placeholderItem = this.getPlaceholderItemRenderContent();
        const selectionItems = this.getSelectionItemsRenderContent();

        return (
            <TextField
                select
                fullWidth
                value={this.props.currentValue}
                SelectProps={{
                    className: this.props.classes.selectField,
                    displayEmpty: this.shouldDisplayEmptyItems()
                }}

                error={!!this.props.currentError}
                helperText={this.props.currentError}

                onChange={this.handlePropertyValueChange}

                component={this.props.rootComponent}
                {...this.props.rootProps}
            >
                {placeholderItem}
                {selectionItems}
            </TextField>
        );
    }

    // endregion
}

export default SelectProperty;
