import { AsShape, ReactChildProp } from 'utils';
import { ClassNameMap, StyledComponentProps } from '@material-ui/core/styles/withStyles';
import { FormControlProps } from '@material-ui/core/es/FormControl';
import { SchemaProperty } from 'SchemaProperty';
import { UiSchema } from 'UiJSONSchema6';
import * as React from 'react';

declare namespace SelectProperty {
    type Shape = AsShape<Props>;

    type PartialProps = Partial<Props>;
    type PartialState = Partial<State>;

    type ClassesMap = Record<ClassesKey, object>;
    type ClassesKey =
        | 'selectField'
        ;

    interface Props extends SchemaProperty.Common.Props, StyledComponentProps<ClassesKey>, ReactChildProp {
        classes?: Partial<ClassNameMap<ClassesKey>>;

        /** @inheritDoc */ propertyName: string;
        /** @inheritDoc */ propertySchema: UiSchema.JSONv6;
        /** @inheritDoc */ rootProps?: object;
        /** @inheritDoc */ rootComponent?: React.ReactType<FormControlProps>;
        /** @inheritDoc */ actSelected?: boolean;
        /** @inheritDoc */ currentValue: string;
        /** @inheritDoc */ currentError: string;
        /** @inheritDoc */ onValueChange: (event: SchemaProperty.PropertyValueChangeEvent) => void;
    }

    interface State {
        /**
         * The items that the user can select from.
         *
         * This is a key-value map.
         */
        items: { [key: string]: string };
        /**
         * The helper text to display for this component.
         */
        helperText?: string;
    }
}

/**
 * `SelectProperty` Component.
 */
declare const SelectProperty: React.ComponentType<SelectProperty.Props>;
/// <reference path="SelectProperty.jsx" />

export default SelectProperty;
