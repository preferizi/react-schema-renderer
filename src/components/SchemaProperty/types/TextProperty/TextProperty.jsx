import TextField from '@material-ui/core/es/TextField/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import autobind from 'autobind';
import React from 'react';
import schemaPropertySharedPropTypes from 'react-schema-renderer/components/SchemaProperty/schemaPropertySharedPropTypes';
import UiSchemaHelper from 'react-schema-renderer/utils/UiSchemaHelper';

/**
 * `TextProperty` Component.
 */
@withStyles(theme => TextProperty.styles(theme))
class TextProperty extends React.Component {
    // region prop types
    /**
     * @inheritDoc
     *
     * @type {TextProperty.Shape}
     */
    static propTypes = {
        ...schemaPropertySharedPropTypes
    };

    /**
     * @inheritDoc
     *
     * @type {TextProperty.PartialProps}
     */
    static defaultProps = {};
    // endregion
    // region styles
    /**
     *
     * @type {function(Theme): TextProperty.ClassesMap}
     */
    static styles = theme => ({});
    // endregion
    // region properties
    /**
     * @type {debug.IDebugger}
     * @private
     */
    #logger = require('debug')(this.constructor.name);

    /**
     * @inheritDoc
     *
     * @type {TextProperty.Props}
     */
    props;

    /**
     * @inheritDoc
     *
     * @type {TextProperty.State}
     */
    state = {};

    // endregion
    /**
     * Tests if this component should be used to render the given schema property.
     *
     * @param {string} propertyName
     * @param {UiSchema.JSONv6} propertySchema
     *
     * @return {boolean}
     * @static
     */
    static shouldUseComponentForSchemaPropertyTester(propertyName, propertySchema) {
        return UiSchemaHelper.getSchemaType(propertySchema) === 'string';
    }

    // region setState override
    /**
     * @inheritDoc
     *
     * Override that lets WebStorm be clever.
     *
     * @param {TextProperty.PartialState} state
     * @param [callback]
     */
    setState(state, callback) {
        super.setState(state, callback);
    }

    // endregion

    /**
     * Gets the text to use for the placeholder.
     *
     * @return {string}
     */
    getSchemaPlaceholderText() {
        return UiSchemaHelper.getSchemaPlaceholderText(this.props.propertySchema);
    }

    /**
     * Gets the title for this components schema.
     *
     * @return {?string}
     */
    getSchemaTitle() {
        return UiSchemaHelper.getSchemaTitle(this.props.propertySchema, this.props.propertyName);
    }

    /**
     * Gets the helper text for this property definition.
     *
     * Based off the value of the `uiHelperText` field.
     *
     * @return {string}
     */
    getSchemaHelperText() {
        return UiSchemaHelper.getSchemaHelperText(this.props.propertySchema);
    }

    // region autobound methods
    /**
     * Handles when the value of this property changes.
     *
     * @param {React.ChangeEvent} event
     */
    @autobind
    handlePropertyValueChange(event) {
        this.props.onValueChange({ name: this.props.propertyName, value: event.target.value });
    }

    // endregion
    // region render & get-render-content methods
    render() {
        return (
            <TextField
                fullWidth
                value={this.props.currentValue}

                placeholder={this.getSchemaPlaceholderText()}
                label={this.getSchemaTitle()}
                helperText={this.props.currentError}
                error={!!this.props.currentError}

                InputLabelProps={{
                    shrink: this.props.actSelected || undefined
                }}

                onChange={this.handlePropertyValueChange}

                component={this.props.rootComponent}
                {...this.props.rootProps}
            />
        );
    }

    // endregion
}

export default TextProperty;
