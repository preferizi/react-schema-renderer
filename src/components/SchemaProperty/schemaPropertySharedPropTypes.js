import PropTypes from 'prop-types';

/**
 * PropTypes for the props shared by all `SchemaProperty` components.
 *
 * @type {SchemaProperty.Common.Shape}
 */
const schemaPropertySharedPropTypes = {
    /** @inheritDoc */ propertyName: PropTypes.string.isRequired,
    /** @inheritDoc */ propertySchema: PropTypes.object.isRequired,
    /** @inheritDoc */ rootProps: PropTypes.object,
    /** @inheritDoc */ rootComponent: PropTypes.any,
    /** @inheritDoc */ actSelected: PropTypes.bool,
    /** @inheritDoc */ currentValue: PropTypes.any,
    /** @inheritDoc */ currentError: PropTypes.string,
    /** @inheritDoc */ onValueChange: PropTypes.func.isRequired
};

export default schemaPropertySharedPropTypes;
