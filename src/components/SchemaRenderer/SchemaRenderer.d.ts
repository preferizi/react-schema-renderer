import { AsShape, ReactChildProp } from 'utils';
import { ClassNameMap, StyledComponentProps } from '@material-ui/core/styles/withStyles';
import { FormControlProps } from '@material-ui/core/es/FormControl';
import { SchemaProperty } from '../SchemaProperty/SchemaProperty';
import { AjvErrors } from 'utils/AjvErrors';
import { UiSchema } from 'UiJSONSchema6';
import * as React from 'react';

export declare namespace SchemaRenderer {
    type Shape = AsShape<Props>;
    type PartialProps = Partial<Props>;

    type ClassesMap = Record<ClassesKey, object>;
    type ClassesKey =
        | 'selectField'
        ;

    interface Props extends StyledComponentProps<ClassesKey>, ReactChildProp {
        classes?: Partial<ClassNameMap<ClassesKey>>;

        /**
         * Registrations for components to use for rendering schema properties.
         *
         * Each registration holds a React component, and a selector function that is
         * used to test if a property should be rendered using the corresponding component.
         *
         * The array is iterated over until a Component is returned, or the end of the array is reached.
         */
        schemaPropertyComponentsRegister?: Array<SchemaProperty.ComponentRegistrationRecord>;

        schema: UiSchema.JSONv6;
        data: object;
        errors: AjvErrors.MappedDataErrors;

        /**
         * Passed to the `SchemaProperty`.
         *
         * Collection of props to be applied to the root component of all schema properties.
         *
         * Useful for 'injecting' custom behaviour, for things like editors ;)
         *
         * You can opt to use `Redux` in place of this, for a performance boost.
         */
        rootProps: object;
        /**
         * Passed to the `SchemaProperty`.
         *
         * The component to use as the root for all schema properties.
         *
         * Useful for 'injecting' custom behaviour, for things like editors ;)
         */
        rootComponent: React.ReactType<FormControlProps>;
        /**
         * Passed to the `SchemaProperty`.
         *
         * Marks if the `SchemaProperty`s being rendered should act
         * as if they're currently selected, or in focus.
         *
         * This is the only prop that's explicitly for an editor-like app,
         * as it's the only nice way to allow this functionality without
         * blowing up component performance, or requiring the editor to exist
         * with the renderer itself.
         *
         * Everything else should be do-able using `rootComponent`.
         *
         * Ideally, this will be honored by the `SchemaProperty` component,
         * but it's not guaranteed.
         */
        actSelected?: boolean

        // /**
        //  * Function that is called when the data of the form being rendered changes.
        //  *
        //  * @param {object} data the new data of the form.
        //  * @param {string} changedPropertyName the name of the property that changed.
        //  * @param {UiSchema.JSONv6} changedPropertyData the schema of the property that changed.
        //  */
        // onDataChange: (data: object, changedPropertyName: string, changedPropertyData: UiSchema.JSONv6) => void;
        /**
         * Function that is called when the value of a property from the schema changes.
         *
         * @param {string} propertyName the name of the property that changed.
         * @param {*} propertyValue the new value of the changed property.
         */
        onPropertyValueChange: (propertyName: string, propertyValue: any) => void;

        categories?: Array;
    }

    interface State {
    }
}

/**
 * `SchemaRenderer` Component.
 */
declare const SchemaRenderer: React.ComponentType<SchemaRenderer.Props>;
/// <reference path="SchemaRenderer.jsx" />

export default SchemaRenderer;
