import Grid from '@material-ui/core/es/Grid/Grid';
import withStyles from '@material-ui/core/styles/withStyles';
import autobind from 'autobind';
import PropTypes from 'prop-types';
import React from 'react';
import SchemaProperty from 'react-schema-renderer/components/SchemaProperty/SchemaProperty';
import schemaPropertyCommonRegister from 'react-schema-renderer/components/SchemaProperty/schemaPropertyComponentsCommonRegister';
import SchemaPropertyErrorOverlay from 'react-schema-renderer/components/SchemaPropertyErrorOverlay/SchemaPropertyErrorOverlay';
import { getHighestPriorityKeywordError } from 'react-schema-renderer/utils/mapAjvErrorMessages';

const uuidV4 = require('uuid/v4');

/**
 * `SchemaRenderer` Component.
 */
@withStyles(theme => SchemaRenderer.styles(theme))
class SchemaRenderer extends React.Component {
    // region prop types
    /**
     * @inheritDoc
     *
     * @type {SchemaRenderer.Shape}
     */
    static propTypes = {
        schema: PropTypes.object.isRequired,
        data: PropTypes.object.isRequired,
        rootProps: PropTypes.object,
        // b/c PropTypes is stupid, apparently? (DnD issues)
        rootComponent: PropTypes.any,
        actSelected: PropTypes.bool,
        schemaPropertyComponentsRegister: PropTypes.array,
        onPropertyValueChange: PropTypes.func.isRequired
    };

    /**
     * @inheritDoc
     *
     * @type {SchemaRenderer.PartialProps}
     */
    static defaultProps = {
        schemaPropertyComponentsRegister: schemaPropertyCommonRegister,
        errors: {}
    };
    // endregion
    // region styles
    /**
     *
     * @type {function(Theme): SchemaRenderer.ClassesMap}
     */
    static styles = theme => ({
        selectField: {
            marginTop: '16px'
        },
        selectForEditing: {
            '&:after': {
                position: 'relative',
                content: '" "',
                width: '100%',
                height: '100%',
                top: '0',
                left: '0',
                right: '0',
                bottom: '0',
                backgroundColor: 'rgba(0,0,0,0.5)',
                zIndex: '2',
                cursor: 'pointer'
            }
        }
    });
    // endregion
    // region properties
    /**
     * @type {debug.IDebugger}
     * @private
     */
    #logger = require('debug')(this.constructor.name);

    /**
     * Unique identifier used for tracking this instance of the `SchemaRenderer`.
     *
     * Mainly for debugging, specifically redux
     *
     * @type {string}
     */
    identifier = uuidV4();

    /**
     * @inheritDoc
     *
     * @type {SchemaRenderer.Props}
     */
    props;

    /**
     * @inheritDoc
     *
     * @type {SchemaRenderer.State}
     */
    state = {};

    // endregion
    // region setState override
    /**
     * @inheritDoc
     *
     * Override that lets WebStorm be clever.
     *
     * @param {Partial<SchemaRenderer.State>} state
     * @param [callback]
     */
    setState(state, callback) {
        super.setState(state, callback);
    }

    // endregion
    constructor(props) {
        super(props);
    }

    // region component lifecycle methods
    // endregion

    /**
     * Gets the current value of a schema-defined property from the state.
     *
     * @param {string} propertyName
     *
     * @return {*}
     */
    getSchemaPropertyValue(propertyName) {
        return this.props.data[propertyName];
    }

    /**
     * Sets the current value of a schema-defined property on the state.
     *
     * This method calls `setState`.
     *
     * @param {string} propertyName
     * @param {*} propertyValue
     */
    setSchemaPropertyValue(propertyName, propertyValue) {
        this.props.onPropertyValueChange(propertyName, propertyValue);
    }

    /**
     * Gets the component to use to render the given schema property.
     *
     * @param {string} propertyName
     * @param {UiSchema.JSONv6|boolean} propertySchema
     *
     * @return {?SchemaProperty.SchemaPropertyComponent}
     */
    getComponentForSchemaProperty(propertyName, propertySchema) {
        return this.props.schemaPropertyComponentsRegister.find(item => item.tester(propertyName, propertySchema))?.component;
    }

    /**
     * Gets the `SchemaProperty` component for rendering the given schema property.
     *
     * @param {string} propertyName
     * @param {UiSchema.JSONv6|boolean} propertySchema
     *
     * @return {*}
     */
    getSchemaPropertyAsComponent(propertyName, propertySchema) {
        if (typeof propertySchema === 'boolean') {
            this.#logger(`${this.getSchemaPropertyAsComponent.name}: properties field was a boolean...?`);

            return null;
        }

        // todo: this can ideally eventually be merged into SchemaProperty, as hopefully it'll be sole once Redux is in play
        const propertyComponent = this.getComponentForSchemaProperty(propertyName, propertySchema);

        if (!propertyComponent) {
            return null;
        }

        return (
            <SchemaPropertyErrorOverlay
                propertyName={propertyName}
                manuallyErrored={!propertySchema}
            >
                <SchemaProperty
                    key={propertyName}

                    propertyComponent={propertyComponent}

                    propertyName={propertyName}
                    propertySchema={propertySchema}

                    rootComponent={this.props.rootComponent}
                    rootProps={this.props.rootProps}
                    actSelected={this.props.actSelected}

                    currentValue={this.getSchemaPropertyValue(propertyName) || ''}
                    currentError={this.getErrorMessagesForProperty(propertyName)?.message || ''}

                    onValueChange={this.handleValueChange}
                />
            </SchemaPropertyErrorOverlay>
        );
    }

    /**
     *
     * @param {string} propertyName
     *
     * @return {?AjvErrors.MappedError}
     */
    getErrorMessagesForProperty(propertyName) {
        /** @type {AjvErrors.MappedDataErrors} */
        const mappedAjvDataErrors = this.props.errors;

        // todo: we're taking the first one off the top - this is 'not right'
        const errorKey = Object.keys(mappedAjvDataErrors)
                               .find(key => key.startsWith(`/${propertyName}`));

        if (!errorKey) {
            return null;
        }

        return getHighestPriorityKeywordError(mappedAjvDataErrors[errorKey]);
    }

    // region autobound methods
    /**
     * Handles when the value of the name `TextField` component changes.
     *
     * @param {React.ChangeEvent} event
     */
    @autobind
    handleNameTextFieldChange(event) {
        this.setState({ resourceName: event.target.value });
    }

    /**
     * Handles when the value of the category `Select` component changes.
     *
     * @param {React.ChangeEvent} event
     */
    @autobind
    handleCategorySelectChange(event) {
        this.setState({ selectedCategory: event.target.value });
    }

    /**
     * Handles when the value of a schema-defined property changes.
     *
     * @param {SchemaProperty.ChangeEvent} event
     */
    @autobind
    handleValueChange(event) {
        // this.#logger(`value of ${event.name} changed to ${event.value}`);

        this.setSchemaPropertyValue(event.name, event.value);
    }

    // endregion
    // region render & get-render-content methods
    /**
     * @param {Array<UiSchema.UiColumn>} uiOrder
     *
     * @return {*}
     */
    mapSchemaUiOrder(uiOrder) {
        const uiColumns = uiOrder.map((uiColumn, index) => this.mapSchemaUiColumn(uiColumn, uiOrder.length, index))
                                 .filter(uiColumn => !!uiColumn);

        if (!uiColumns.length) {
            return null;
        }

        return (
            <Grid container spacing={16}>
                {uiColumns}
            </Grid>
        );
    }

    /**
     *
     * @param {UiSchema.UiColumn} uiColumn
     * @param {number} numOfColumns the number of columns that are being rendered in this form.
     * @param {string|number} key
     *
     * @return {*}
     */
    mapSchemaUiColumn(uiColumn, numOfColumns, key) {
        const uiRows = uiColumn.map((uiRow, index) => this.mapSchemaUiRow(uiRow, index))
                               .filter(uiRow => !!uiRow);

        if (!uiRows.length) {
            return null;
        }

        return (
            <Grid item key={key} xs={12} md={12 / numOfColumns}>
                {uiRows}
            </Grid>
        );
    }

    /**
     * Maps a `UiRow` to components.
     *
     * @param {UiSchema.UiRow} uiRow
     * @param {string|number} key
     *
     * @return {*}
     */
    mapSchemaUiRow(uiRow, key) {
        const uiSlots = uiRow.map((uiSlot, index) => this.mapSchemaUiSlot(uiSlot, uiRow.length, index))
                             .filter(uiSlot => !!uiSlot);

        if (!uiSlots.length) {
            return null;
        }

        return (
            <Grid container spacing={16} key={key}>
                {uiSlots}
            </Grid>
        );
    }

    /**
     * Maps a `UiSlot` to a component.
     *
     * @param {UiSchema.UiSlot} uiSlot
     * @param {number} numOfSlots the number of slots being rendered in this row.
     * @param {string|number} key
     *
     * @return {*}
     */
    mapSchemaUiSlot(uiSlot, numOfSlots, key) {
        const propertyComponent = this.getSchemaPropertyAsComponent(uiSlot, this.props.schema.properties[uiSlot]);

        if (!propertyComponent) {
            return null;
        }

        return (
            <Grid item key={key} xs={12} md={12 / numOfSlots}>
                {propertyComponent}
            </Grid>
        );
    }

    render() {
        console.log('render');

        if (!this.props.schema.properties) {
            this.#logger('warning: schema doesn\'t have properties!');

            return null;
        }

        const schemaProperties = this.props.schema.properties;

        const fallback = [Object.keys(schemaProperties).map(key => [key])];

        /** @type {Array<UiSchema.UiColumn>} */
        const desiredOrder = this.props.schema.uiOrder || fallback;

        return (
            <Grid container data-identifier={this.identifier}>
                {this.mapSchemaUiOrder(desiredOrder)}
            </Grid>
        );
    }

    // endregion
}

export default SchemaRenderer;
