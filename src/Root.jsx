import Card from '@material-ui/core/es/Card/Card';
import CardContent from '@material-ui/core/es/CardContent/CardContent';
import withStyles from '@material-ui/core/es/styles/withStyles';
import React from 'react';
import SchemaRenderer from 'react-schema-renderer/components/SchemaRenderer/SchemaRenderer';

const vxSchema = require('react-schema-renderer/schemas/test.schema.json');

/**
 * Root wrapper component required for `state`.
 */
@withStyles(theme => Root.styles(theme))
class Root extends React.Component {
    // region styles
    /**
     *
     * @type {function(Theme): Root.ClassesMap}
     */
    static styles = theme => ({
        root: {
            padding: theme.spacing.unit * 2
        }
    });
    // endregion

    state = {
        viewerSchema: vxSchema,
        viewerData: {}
    };

    render() {
        return (
            <div className={this.props.classes.root}>
                <Card>
                    <CardContent>
                        <SchemaRenderer
                            schema={this.state.viewerSchema}
                            data={this.state.viewerData}

                            onPropertyValueChange={(propertyName, propertyValue) => {
                                this.setState({
                                    viewerData: {
                                        ...this.state.viewerData,
                                        [propertyName]: propertyValue
                                    }
                                });
                            }}
                        />
                    </CardContent>
                </Card>
            </div>
        );
    }
}

export default Root;
