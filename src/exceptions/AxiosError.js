class AxiosError {
    static interceptorWrapper(error) {
        throw new AxiosError(error);
    }

    constructor(error) {
        Object.assign(this, error);
    }
}

export default AxiosError;
