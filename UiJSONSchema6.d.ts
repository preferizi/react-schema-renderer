type UiJSONSchema6 = UiSchema.JSONv6;

export declare namespace UiSchema {
    export type UiTypeName =
        | JSONv6TypeName
        | 'checkbox'
        | 'switch'
        | 'file'
        ;

    export type JSONv6TypeName =
        | 'string'
        | 'number'
        | 'integer'
        | 'boolean'
        | 'object'
        | 'array'
        | 'null'
        | 'any'
        ;

    export type JSONv6Type = any[] | boolean | number | null | object | string

    export type JSONv6Key = keyof UiJSONSchema6;

    /**
     * Ui JSON Schema V6
     * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01
     */
    export interface JSONv6 {
        $id?: string
        $ref?: string
        $schema?: 'http://json-schema.org/schema#' | 'http://json-schema.org/hyper-schema#' |
            'http://json-schema.org/draft-06/schema#' | 'http://json-schema.org/draft-06/hyper-schema#'

        /**
         * Must be strictly greater than 0.
         * A numeric instance is valid only if division by this keyword's value results in an integer.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.1
         */
        multipleOf?: number

        /**
         * Representing an inclusive upper limit for a numeric instance.
         * This keyword validates only if the instance is less than or exactly equal to "maximum".
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.2
         */
        maximum?: number

        /**
         * Representing an exclusive upper limit for a numeric instance.
         * This keyword validates only if the instance is strictly less than (not equal to) to "exclusiveMaximum".
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.3
         */
        exclusiveMaximum?: number

        /**
         * Representing an inclusive lower limit for a numeric instance.
         * This keyword validates only if the instance is greater than or exactly equal to "minimum".
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.4
         */
        minimum?: number

        /**
         * Representing an exclusive lower limit for a numeric instance.
         * This keyword validates only if the instance is strictly greater than (not equal to) to "exclusiveMinimum".
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.5
         */
        exclusiveMinimum?: number

        /**
         * Must be a non-negative integer.
         * A string instance is valid against this keyword if its length is less than, or equal to, the value of this keyword.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.6
         */
        maxLength?: number

        /**
         * Must be a non-negative integer.
         * A string instance is valid against this keyword if its length is greater than, or equal to, the value of this keyword.
         * Omitting this keyword has the same behavior as a value of 0.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.7
         */
        minLength?: number

        /**
         * Should be a valid regular expression, according to the ECMA 262 regular expression dialect.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.8
         */
        pattern?: string

        /**
         * This keyword determines how child instances validate for arrays, and does not directly validate the immediate instance itself.
         * Omitting this keyword has the same behavior as an empty schema.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.9
         */
        items?: boolean | UiJSONSchema6 | UiJSONSchema6[]

        /**
         * This keyword determines how child instances validate for arrays, and does not directly validate the immediate instance itself.
         * If "items" is an array of schemas, validation succeeds if every instance element
         * at a position greater than the size of "items" validates against "additionalItems".
         * Otherwise, "additionalItems" MUST be ignored, as the "items" schema
         * (possibly the default value of an empty schema) is applied to all elements.
         * Omitting this keyword has the same behavior as an empty schema.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.10
         */
        additionalItems?: boolean | UiJSONSchema6

        /**
         * Must be a non-negative integer.
         * An array instance is valid against "maxItems" if its size is less than, or equal to, the value of this keyword.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.11
         */
        maxItems?: number

        /**
         * Must be a non-negative integer.
         * An array instance is valid against "maxItems" if its size is greater than, or equal to, the value of this keyword.
         * Omitting this keyword has the same behavior as a value of 0.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.12
         */
        minItems?: number

        /**
         * If this keyword has boolean value false, the instance validates successfully.
         * If it has boolean value true, the instance validates successfully if all of its elements are unique.
         * Omitting this keyword has the same behavior as a value of false.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.13
         */
        uniqueItems?: boolean

        /**
         * An array instance is valid against "contains" if at least one of its elements is valid against the given schema.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.14
         */
        contains?: boolean | UiJSONSchema6

        /**
         * Must be a non-negative integer.
         * An object instance is valid against "maxProperties" if its number of properties is less than, or equal to, the value of this keyword.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.15
         */
        maxProperties?: number

        /**
         * Must be a non-negative integer.
         * An object instance is valid against "maxProperties" if its number of properties is greater than,
         * or equal to, the value of this keyword.
         * Omitting this keyword has the same behavior as a value of 0.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.16
         */
        minProperties?: number

        /**
         * Elements of this array must be unique.
         * An object instance is valid against this keyword if every item in the array is the name of a property in the instance.
         * Omitting this keyword has the same behavior as an empty array.
         *
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.17
         */
        required?: string[]

        /**
         * This keyword determines how child instances validate for objects, and does not directly validate the immediate instance itself.
         * Validation succeeds if, for each name that appears in both the instance and as a name within this keyword's value,
         * the child instance for that name successfully validates against the corresponding schema.
         * Omitting this keyword has the same behavior as an empty object.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.18
         */
        properties?: {
            [k: string]: boolean | UiJSONSchema6
        }

        /**
         * This attribute is an object that defines the schema for a set of property names of an object instance.
         * The name of each property of this attribute's object is a regular expression pattern in the ECMA 262, while the value is a schema.
         * If the pattern matches the name of a property on the instance object, the value of the instance's property
         * MUST be valid against the pattern name's schema value.
         * Omitting this keyword has the same behavior as an empty object.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.19
         */
        patternProperties?: {
            [k: string]: boolean | UiJSONSchema6
        }

        /**
         * This attribute defines a schema for all properties that are not explicitly defined in an object type definition.
         * If specified, the value MUST be a schema or a boolean.
         * If false is provided, no additional properties are allowed beyond the properties defined in the schema.
         * The default value is an empty schema which allows any value for additional properties.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.20
         */
        additionalProperties?: boolean | UiJSONSchema6

        /**
         * This keyword specifies rules that are evaluated if the instance is an object and contains a certain property.
         * Each property specifies a dependency.
         * If the dependency value is an array, each element in the array must be unique.
         * Omitting this keyword has the same behavior as an empty object.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.21
         */
        dependencies?: {
            [k: string]: boolean | UiJSONSchema6 | string[]
        }

        /**
         * Takes a schema which validates the names of all properties rather than their values.
         * Note the property name that the schema is testing will always be a string.
         * Omitting this keyword has the same behavior as an empty schema.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.22
         */
        propertyNames?: boolean | UiJSONSchema6

        /**
         * This provides an enumeration of all possible values that are valid
         * for the instance property. This MUST be an array, and each item in
         * the array represents a possible value for the instance value. If
         * this attribute is defined, the instance value MUST be one of the
         * values in the array in order for the schema to be valid.
         *
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.23
         */
        enum?: UiSchema.JSONv6Type[]

        /**
         * More readible form of a one-element "enum"
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.24
         */
        const?: UiSchema.JSONv6Type

        /**
         * A single type, or a union of simple types
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.25
         */
        type?: UiSchema.JSONv6TypeName | UiSchema.JSONv6TypeName[]

        /**
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.26
         */
        allOf?: UiJSONSchema6[]

        /**
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.27
         */
        anyOf?: UiJSONSchema6[]

        /**
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.28
         */
        oneOf?: UiJSONSchema6[]

        /**
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-6.29
         */
        not?: boolean | UiJSONSchema6

        /**
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-7.1
         */
        definitions?: {
            [k: string]: boolean | UiJSONSchema6
        }

        /**
         * This attribute is a string that provides a short description of the instance property.
         *
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-7.2
         */
        title?: string

        /**
         * This attribute is a string that provides a full description of the of purpose the instance property.
         *
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-7.2
         */
        description?: string

        /**
         * This keyword can be used to supply a default JSON value associated with a particular schema.
         * It is RECOMMENDED that a default value be valid against the associated schema.
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-7.3
         */
        default?: UiSchema.JSONv6Type

        /**
         * Array of examples with no validation effect the value of "default" is usable as an example without repeating it under this keyword
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-7.4
         */
        examples?: UiSchema.JSONv6Type[]

        /**
         * @see https://tools.ietf.org/html/draft-wright-json-schema-validation-01#section-8
         */
        format?: string

        /* Ui stuff goes here */
        // todo: completely subject to change - highly unstable

        /**
         * The ui type of this property.
         *
         * Takes priority over the "type" field.
         */
        uiType?: UiTypeName;

        /**
         * The title for the ui component representing this property.
         *
         * Takes priority over the "title" field.
         *
         * If this field is null, no title should be displayed.
         * Otherwise, if neither `title` or `uiTitle` are provided,
         * the properties key should be used (after transformation).
         */
        uiTitle?: string | null;

        /**
         * The description for the ui component representing this property.
         *
         * Takes priority over the "description" field.
         *
         * This is usually shown as the help text for most components.
         */
        uiDescription?: string;

        /**
         * Defines the order that the components for properties should be rendered in.
         *
         * This field is an array of `UiColumn`s, which are themselves an array made
         * up of `UiRow`s, that are in turn an array of `UiSlot`s, which are strings
         * expected to map to property names.
         */
        uiOrder?: Array<UiColumn>;

        /**
         * The helper text to show under or around the ui component for properties.
         */
        uiHelperText?: string;

        /**
         * Text to show as a placeholder value, when applicable.
         */
        uiPlaceholderText?: string;

        /**
         * An object containing the labels to use for enum values.
         *
         * If an enum is rendered, and a label exists, that label will be used
         * in place of the enums value where it's displayed.
         *
         * The submitted value will remain unchanged.
         */
        uiEnumLabels?: { [key: string]: string }
        uiMessages?: UiMessages;
    }

    type UiMessages = UiMessages.UiMessages | string;

    namespace UiMessages {
        type UiMessages = {
            [key in JSONv6Key]: string | UiMessages;
        }
    }

    interface UiOrderCoordinates {
        columnIndex: number;
        rowIndex: number;
        slotIndex: number;
    }

    /**
     * Describes the properties that make up a `UiColumn`.
     *
     * Used with `uiOrder` field to denote the order & grouping of property components.
     */
    type UiColumn = Array<UiRow>;

    /**
     * Describes the properties that make up a `UiRow`.
     *
     * Used with `uiOrder` to denote the order & grouping of property components.
     */
    type UiRow = Array<UiSlot>;

    /**
     * Denotes the order a property component should be situated in a `UiRow`.
     *
     * This value should be the name of a property described by the `properties`
     * field in the `UiJSONSchema` object.
     *
     * Used with `uiOrder` to denote the order & grouping of property components.
     */
    type UiSlot = string;
}
