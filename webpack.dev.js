process.env.NODE_ENV = 'development';
process.env.BABEL_ENV = 'development';

const webpack = require('webpack');

const merge = require('webpack-merge');

const common = require('./webpack.common');
const cdn = require('./webpack.cdn');

module.exports = merge(common, cdn, {
    mode: 'development',
    devtool: 'eval-source-map',
});

const history = require('connect-history-api-fallback');
const convert = require('koa-connect');

const webpackServeWaitPage = require('webpack-serve-waitpage');

module.exports.serve = {
    port: 3334,
    clipboard: false,
    logTime: false,
    content: [__dirname],
    add: (app, middleware, options) => {
        app.use(convert(history({})));
        app.use(webpackServeWaitPage(options, { theme: 'material' }));
    }
};
