const componentName = process.argv[2];

if (!componentName || typeof componentName !== 'string') {
    throw new Error('expected component name as the first argument, got nothing');
}

// region definitionFile
const definitionFile = `
import { AsShape, ReactChildProp } from 'utils';
import { ClassNameMap, StyledComponentProps } from '@material-ui/core/styles/withStyles';
import * as React from 'react';

declare namespace TemplateComponent {
    type Shape = AsShape<Props>;

    type PartialProps = Partial<Props>;
    type PartialState = Partial<State>;

    type ClassesMap = Record<ClassesKey, object>;
    type ClassesKey =
        | 'myClass'
        ;

    interface Props extends StyledComponentProps<ClassesKey>, ReactChildProp {
        classes?: Partial<ClassNameMap<ClassesKey>>;
    }

    interface State {
    }
}

/**
 * TemplateComponent Component.
 *
 * This is a template of how to structure components & views.
 *
 * 1. Copy this file along with its typing definitions file (that's the .d.ts file).
 * 2. Rename the copied jsx *file* - WebStorm will pickup that it's a class,
 *      and so offer to rename the 'Class and its usages' for you as well.
 * 3. Rename the \`.d.ts\` type definition file; **BE AWARE** WebStorm will consider the
 *      '.d.' to be part of the filename, and will select this for you by mistake.
 *      If you remove the \`.d.\`, it'll make the file a compilable TypeScript file,
 *      instead of a type definition module.
 * 4. ???
 * 5. Profit!!
 */
declare const TemplateComponent: React.ComponentType<TemplateComponent.Props>;
/// <reference path="TemplateComponent.jsx" />

export default TemplateComponent;
`.trimLeft();
// endregion
// region jsxFile
const jsxFile = `
import withStyles from '@material-ui/core/styles/withStyles';
import React from 'react';

/**
 * TemplateComponent Component.
 *
 * This is a template of how to structure components & views.
 *
 * 1. Copy this file along with its typing definitions file (that's the .d.ts file).
 * 2. Rename the copied jsx *file* - WebStorm will pickup that it's a class,
 *      and so offer to rename the 'Class and its usages' for you as well.
 * 3. Rename the \`.d.ts\` type definition file; **BE AWARE** WebStorm will consider the
 *      '.d.' to be part of the filename, and will select this for you by mistake.
 *      If you remove the \`.d.\`, it'll make the file a compilable TypeScript file,
 *      instead of a type definition module.
 * 4. ???
 * 5. Profit!!
 *
 * For bonus points, remove the \`@withStyles\` & region comments if you're not using them ;)
 */
@withStyles(theme => TemplateComponent.styles(theme))
class TemplateComponent extends React.Component {
    // region prop types
    /**
     * @inheritDoc
     *
     * @type {TemplateComponent.Shape}
     */
    static propTypes = {};

    /**
     * @inheritDoc
     *
     * @type {TemplateComponent.PartialProps}
     */
    static defaultProps = {};
    // endregion
    // region styles
    /**
     *
     * @type {function(Theme): TemplateComponent.ClassesMap}
     */
    static styles = theme => ({});
    // endregion
    // region properties
    /**
     * @type {debug.IDebugger}
     * @private
     */
    #logger = require('debug')(this.constructor.name);

    /**
     * @inheritDoc
     *
     * @type {TemplateComponent.Props}
     */
    props;

    /**
     * @inheritDoc
     *
     * @type {TemplateComponent.State}
     */
    state = {};
    // endregion
    // region setState override
    /**
     * @inheritDoc
     *
     * Override that lets WebStorm be clever.
     *
     * @param {TemplateComponent.PartialState} state
     * @param [callback]
     */
    setState(state, callback) {
        super.setState(state, callback);
    }

    // endregion
    // region component lifecycle methods
    // endregion
    // region autobound methods
    // endregion
    // region render & get-render-content methods
    render() {
        return new Error('Don\\'t let Gareth catch you trying to use me directly ;)');
    }

    // endregion
}

export default TemplateComponent;
`.trimLeft();
// endregion

const fs = require('fs');
const path = require('path');

fs.mkdirSync(componentName);

fs.writeFileSync(
    path.join(componentName, `${componentName}.d.ts`),
    definitionFile.replace(/TemplateComponent/g, componentName)
);
fs.writeFileSync(
    path.join(componentName, `${componentName}.jsx`),
    jsxFile.replace(/TemplateComponent/g, componentName)
);
