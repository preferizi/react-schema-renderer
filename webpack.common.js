const path = require('path');
const webpack = require('webpack');

const nodePackage = require('./package');

const DotEnvPlugin = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// use BUST_CACHE if it's explicitly define; otherwise, check if production build
const cacheBust = process.env.BUST_CACHE === undefined
    ? process.env.NODE_ENV === 'production'
    : process.env.BUST_CACHE;

module.exports = {
    cache: true,
    resolve: {
        extensions: ['.js', '.json', '.jsx'],
        alias: {
            autobind: path.resolve(__dirname, 'src/utils/autobind.js'),
            [nodePackage.name]: path.resolve(__dirname, 'src')
        }
    },
    entry: {
        bundle: path.resolve('src/index.jsx')
    },
    output: {
        path: path.resolve('build'),
        filename: `static/js/[name].${cacheBust ? '[chunkhash]' : 'lackluster'}.js`,
        publicPath: '/'
    },
    performance: {
        maxAssetSize: 5 * 1024 * 1024,
        maxEntrypointSize: 5 * 1024 * 1024
    },
    plugins: [
        new DotEnvPlugin(),
        new HtmlWebpackPlugin({
            cache: true,
            inject: true,
            template: path.resolve('public/index.html'),
            title: 'The Races'
        }),
        new webpack.HashedModuleIdsPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
        })
    ],
    optimization: {
        noEmitOnErrors: true,
        splitChunks: {
            chunks: 'all'
        }
    },
    module: {
        rules: [{
            test: /\.css$/,
            use: [
                { loader: 'style-loader' },
                { loader: 'css-loader', options: { importLoaders: 1 } },
                { loader: 'postcss-loader', options: { plugins: () => [] } }
            ]
        }, {
            test: /\.(gif|png|jpe?g|svg)$/i,
            use: [
                {
                    loader: 'file-loader',
                    options: {}
                }
            ]
        }, {
            test: /\.(js|jsx)$/,
            exclude: /node_modules\/(?!(query-string|strict-uri-encode)\/).*/,
            include: [
                /(src|test)/,
                path.join(__dirname, 'node_modules', 'query-string')
            ],
            loader: 'babel-loader',
            options: {
                presets: ['@babel/react'],
                plugins: [
                    ['@babel/proposal-decorators', { 'legacy': true }], // <3<3<3
                    ['@babel/proposal-optional-chaining'], // 4/5
                    ['@babel/proposal-class-properties'], // 4/5
                    ['@babel/proposal-object-rest-spread'], // :'(
                    ['import', { libraryName: '@material-ui/core', libraryDirectory: 'es', camel2DashComponentName: false }, '@material-ui/core'],
                    ['import', { libraryName: '@material-ui/icons', libraryDirectory: 'es', camel2DashComponentName: false }, '@material-ui/icons']
                ]
            }
        }]
    },
    stats: {
        assetsSort: '!chunks'
    },
    node: {
        fs: 'empty'
    }
};
