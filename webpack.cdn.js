const WebpackCdnPlugin = require('webpack-cdn-plugin');

const isProduction = () => process.env.NODE_ENV === 'production';

/*
    Add any modules that can be imported via cdn here.

    the cdn lowers the size of the vendor bundle at a minor cost of ~100kb to the bundle bundle.

    you can cdn *any* package, so long as it's in the *root* node_modules,
    regardless of it's direct dependency of this project or not.

    packages are only placed in the *root* of the node_module tree if
        a. they're a direct dependency of this project, or
        b. they're a dependency of two or more packages within the root node_modules

    this is important to keep in mind, as it means you can't cdn all packages.
 */

module.exports = {
    plugins: [
        new WebpackCdnPlugin({
            modules: [
                {
                    name: 'react',
                    var: 'React',
                    path: isProduction()
                        ? 'umd/react.production.min.js'
                        : 'umd/react.development.js'
                },
                {
                    name: 'react-dom',
                    var: 'ReactDOM',
                    path: isProduction()
                        ? 'umd/react-dom.production.min.js'
                        : 'umd/react-dom.development.js'
                },
                {
                    name: 'bn.js',
                    var: 'BN',
                    path: isProduction()
                        ? 'lib/bn.js'
                        : 'lib/bn.js'
                },
                {
                    name: 'prop-types',
                    var: 'PropTypes',
                    path: isProduction()
                        ? 'prop-types.min.js'
                        : 'prop-types.js'
                }
            ],
            publicPath: '/node_modules'
        })
    ]
};
