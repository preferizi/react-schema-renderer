import * as React from 'react';

export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

/**
 * Construct a type with a set of properties from O of type T.
 */
export type MapToWithType<O, T> = { [P in keyof O]: T; };

/**
 * Construct a shape with a set of properties from O of type `React.ReactPropTypes | React.Validator`.
 */
export type AsShape<O> = MapToWithType<O, React.ReactPropTypes | React.Validator>

/**
 * Object that contains the common `children` property, for component props.
 */
export type ReactChildProp = { children?: React.ReactNode; };
