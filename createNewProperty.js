const propertyName = process.argv[2];

if (!propertyName || typeof propertyName !== 'string') {
    throw new Error('expected property name as the first argument, got nothing');
}

// region definitionFile
const definitionFile = `
import { AsShape, ReactChildProp } from 'utils';
import { ClassNameMap, StyledComponentProps } from '@material-ui/core/styles/withStyles';
import { FormControlProps } from '@material-ui/core/es/FormControl';
import { SchemaProperty } from 'SchemaProperty';
import { UiSchema } from 'UiJSONSchema6';
import * as React from 'react';

declare namespace TemplateProperty {
    type Shape = AsShape<Props>;

    type PartialProps = Partial<Props>;
    type PartialState = Partial<State>;

    type ClassesMap = Record<ClassesKey, object>;
    type ClassesKey =
        | 'myClass'
        ;

    interface Props extends SchemaProperty.Common.Props, ClassNameMap, StyledComponentProps<ClassesKey>, ReactChildProp {
        classes?: Partial<ClassNameMap<ClassesKey>>;

        /** @inheritDoc */ propertyName: string;
        /** @inheritDoc */ propertySchema: UiSchema.JSONv6;
        /** @inheritDoc */ rootProps?: object;
        /** @inheritDoc */ rootComponent?: React.ReactType<FormControlProps>;
        /** @inheritDoc */ actSelected?: boolean
        /** @inheritDoc */ currentValue: string;
        /** @inheritDoc */ currentError: string;
        /** @inheritDoc */ onValueChange: (event: SchemaProperty.PropertyValueChangeEvent) => void;
    }

    interface State {
    }
}

/**
 * \`TemplateProperty\` Component.
 */
declare const TemplateProperty: React.ComponentType<TemplateProperty.Props>;
/// <reference path="TemplateProperty.jsx" />

export default TemplateProperty;
`.trimLeft();
// endregion
// region jsxFile
const jsxFile = `
import FormControl from '@material-ui/core/es/FormControl/FormControl';
import withStyles from '@material-ui/core/styles/withStyles';
import autobind from 'autobind';
import React from 'react';
import schemaPropertySharedPropTypes from 'react-schema-renderer/components/SchemaProperty/schemaPropertySharedPropTypes';

/**
 * \`SelectProperty\` Component.
 */
@withStyles(theme => TemplateProperty.styles(theme))
class TemplateProperty extends React.Component {
    // region prop types
    /**
     * @inheritDoc
     *
     * @type {TemplateProperty.Shape}
     */
    static propTypes = {
        ...schemaPropertySharedPropTypes
    };

    /**
     * @inheritDoc
     *
     * @type {TemplateProperty.PartialProps}
     */
    static defaultProps = {};
    // endregion
    // region styles
    /**
     *
     * @type {function(Theme): TemplateProperty.ClassesMap}
     */
    static styles = theme => ({});
    // endregion
    // region properties
    /**
     * @type {debug.IDebugger}
     * @private
     */
    #logger = require('debug')(this.constructor.name);

    /**
     * @inheritDoc
     *
     * @type {TemplateProperty.Props}
     */
    props;

    /**
     * @inheritDoc
     *
     * @type {TemplateProperty.State}
     */
    state = {
        items: {}
    };
    // endregion
    // region shouldUseComponentForSchemaPropertyTester
    /**
     * Tests if this component should be used to render the given schema property.
     *
     * @param {string} propertyName
     * @param {UiSchema.JSONv6} propertySchema
     *
     * @return {boolean}
     * @static
     */
    static shouldUseComponentForSchemaPropertyTester(propertyName, propertySchema) {
        return new Error('you have to define a condition to test for!');
    }
    // endregion
    // region setState override
    /**
     * @inheritDoc
     *
     * Override that lets WebStorm be clever.
     *
     * @param {TemplateProperty.PartialState} state
     * @param [callback]
     */
    setState(state, callback) {
        super.setState(state, callback);
    }

    // endregion
    // region autobound methods
    /**
     * Handles when the value of this property changes.
     *
     * @param {React.ChangeEvent} event
     */
    @autobind
    handlePropertyValueChange(event) {
        this.props.onValueChange({ name: this.props.propertyName, value: event.target.value });
    }

    // endregion
    // region render & get-render-content methods

    render() {
        /*
            be sure to use:
                this.props.currentValue
                this.props.rootComponent
                this.props.errorMessage
                {...this.props.rootProps}

            preferably also add support for:
                this.props.actSelected
                
            Use FormControl as your root element,
            unless you know what you're doing!
         */
        return (
            <FormControl
                component${'='}{this.props.rootComponent}
                error${'='}{!!this.props.errorMessage}

                {...this.props.rootProps}
            >
                {new Error('Don\\'t let Gareth catch you trying to use me directly ;)')}
            </FormControl>
        );
    }

    // endregion
}

export default TemplateProperty;
`.trimLeft();
// endregion

const fs = require('fs');
const path = require('path');

fs.mkdirSync(propertyName);

fs.writeFileSync(
    path.join(propertyName, `${propertyName}.d.ts`),
    definitionFile.replace(/TemplateProperty/g, propertyName)
);
fs.writeFileSync(
    path.join(propertyName, `${propertyName}.jsx`),
    jsxFile.replace(/TemplateProperty/g, propertyName)
);
